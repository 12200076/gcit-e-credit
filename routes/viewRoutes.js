// const express = require('express')
// const router=express.Router()
// const viewsController=require('./../controllers/viewController')
// const authController=require('./../controllers/authController')

// router.get('/login',viewsController.getLoginForm)
// router.get('/signup',viewsController.getSignupForm)
// router.get('/',viewsController.getHome)
// router.get('/me', authController.protect,viewsController.getProfile)

// module.exports=router


const express = require('express')
const router=express.Router()
const viewsController=require('../controllers/viewController')
const authController = require('./../controllers/authController')

router.get('/login',viewsController.getLoginForm)
router.get('/signup',viewsController.getSignupForm)
router.get('/',viewsController.getHome)
// router.get('/me',authController.protect, viewsController.getProfile)
router.get('/me', viewsController.getProfile)

module.exports=router

