// const mongoose = require('mongoose')
// const validator = require('validator')
// const bcrypt = require('bcryptjs')
  

// const userSchema = new mongoose.Schema({
//     name: {
//         type: String,
//         required: [true, 'Please tell us your name!'],
//     },
//     email: {
//         type: String,
//         required: [true, 'Please provide your email'],
//         unique: true,
//         lowercase: true,
//         validate: [validator.isEmail, 'Please provide a valid email'],
//     },
//     photo: {
//         type: String,
//         default: 'default.jpeg',
//     },
//     role: {
//         type: String,
//         enum: ['user', 'sme', 'e-credit', 'admin'],
//         default: 'user',
//     },
//     password: {
//         type: String,
//         required: [true, 'Please provide a password!'],
//         minlength: 8,
//         select: false,
//     },
//     passwordConfirm: {
//         type: String,
//         required: [true, 'Please confirm your password'],
//         validate: {
//             validator: function (el) {
//                 return el === this.password
//             },
//             message: 'Passwords are not the same',
//         },
//     },
//     active: {
//         type: Boolean, 
//         default: true,
//         select: false,
//     },

    
// });
// // userSchema.pre( 'save', async function (next){
// //     if (!this.isModified('password')) return next()
// // }),

// userSchema.pre( 'save', async function (next){
//     if (!this.isModified('password')) return next()

//     this.password = await bcrypt.hash(this.password, 12)

//     this.passwordConfirm = undefined
//     next()
// });

// userSchema.pre('findOneAndUpdate', async function (next) {
//     const update = this.getUpdate();
//     if (update.password !== '' && 
//         update.password !== undefined && 
//         update.password == update.passwordConfirm) {
  
  
//       // Hash the password with cost of 12
//       this.getUpdate().password = await bcrypt.hash(update.password, 12)
  
  
//       // // Delete passwordConfirm field
//       update.passwordConfirm = undefined
//       next()
//     }else
//     next()
//   })

// const User =  mongoose.model('User', userSchema)
// module.exports = User


const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Please provide your name'],
  },
  email: {
    type: String,
    required: [true, 'Please provide your email'],
    unique: true,
    lowercase: true,
    validate: [validator.isEmail, 'Please provide a valid email'],
  },
  photo: {
    type: String,
    default: 'default.jpg',
  },
  role: {
    type: String,
    enum: ['user', 'sme', 'pharmacist', 'admin'],
    default: 'user',
  },
  password: {
    type: String,
    required: [true, 'Please provide a password'],
    minlength: 8,
    select: false, // Password won't be included in query results
  },
  passwordConfirm: {
    type: String,
    required: [true, 'Please confirm your password'],
    validate: {
      validator: function (el) {
        return el === this.password;
      },
      message: 'Passwords do not match',
    },
  },
  active: {
    type: Boolean,
    default: true,
    select: false, // This field won't be included in query results
  },
});

// Hash password before saving to the database
userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();

  // Hash the password with a cost of 12
  this.password = await bcrypt.hash(this.password, 12);

  // Delete the passwordConfirm field as it's no longer needed
  this.passwordConfirm = undefined;

  next();
});

userSchema.pre('findOneAndUpdate', async function (next) {
  const update = this.getUpdate();

  if (update.password !== '' && update.password !== undefined && update.password === update.passwordConfirm) {
    update.password = await bcrypt.hash(update.password, 12);
    update.passwordConfirm = undefined;
  }
  
  next();
});

// userSchema.methods.correctPassword=async function(
//   candidatePassword,
//   userPassword,
// ){
//   return await bcrypt.compare(candidatePassword,userPassword)
// }
const User = mongoose.model('User', userSchema);

module.exports = User;
