const express = require("express")
const path = require('path')
const app = express()

app.use(express.json())

// const userRouter = require("../../controllers/routes/userRoutes.js")
// const userRouter = require('./routes/userRoutes.js');
const userRouter = require('./routes/userRoutes');
const newsRouter = require('./routes/newsRoutes');

const viewRouter = require('./routes/viewRoutes')
// const cookieParser = require('cookie-parser')
// app.use(cookieParser())


app.use('/api/v1/users', userRouter)
app.use('/api/v1/news', newsRouter)
app.use('/', viewRouter)

app.use(express.static(path.join(__dirname, 'views')))
// const port = 4001
// app.listen(port, () => {
//     console.log(`App runninf on port ${port}..`)
// })

module.exports = app